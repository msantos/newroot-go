package main

import (
	"errors"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"os/exec"
	"os/signal"
	"path"
	"path/filepath"
	"strings"
	"syscall"

	"codeberg.org/msantos/execve"
)

type stateT struct {
	hostname string
	etc      string
	tmp      string
	verbose  bool

	lib        []string
	cloneflags int
	cwd        string
	mountCwd   bool
}

const (
	version = "0.1.0"

	libpath = "/lib:/lib64:/usr/lib:/usr/local/lib"
)

var sysdir = []string{
	"/proc",
	"/dev",
	"/etc",
	"/tmp",
}

func usage() {
	fmt.Fprintf(os.Stderr, `%s v%s
Usage: %s [<option>] <command> <...>

Options:

`, path.Base(os.Args[0]), version, os.Args[0])
	flag.PrintDefaults()
}

func filterPathExists(path []string) []string {
	p := make([]string, 0)

	for _, v := range path {
		if _, err := os.Stat(v); err != nil {
			log.Println("skipping: not found:", v)
			continue
		}
		abs, err := filepath.Abs(v)
		if err != nil {
			log.Fatalln(err)
		}

		p = append(p, abs)
	}

	return p
}

func main() {
	help := flag.Bool("help", false, "Usage")
	verbose := flag.Bool("verbose", false, "Enable logging")
	nonet := flag.Bool("nonet", false, "Disable network")
	nopid := flag.Bool("nopid", false, "Disable global PID namespace")
	noipc := flag.Bool("noipc", false, "Disable global IPC")
	hostname := flag.String("hostname", "", "Set hostname")
	etc := flag.String("etc", "/etc", "Path to /etc directory")
	tmp := flag.String("tmp", "4M", "/tmp size")
	lib := flag.String("lib", libpath, "library directories")

	flag.Usage = func() { usage() }

	flag.Parse()

	if *help {
		usage()
		os.Exit(2)
	}

	cwd, err := os.Getwd()
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(2)
	}

	if !*verbose {
		log.SetOutput(io.Discard)
	}

	cloneflags := syscall.CLONE_NEWUSER | syscall.CLONE_NEWNS

	if *nonet {
		cloneflags |= syscall.CLONE_NEWNET
	}

	if *nopid {
		cloneflags |= syscall.CLONE_NEWPID
	}

	if *noipc {
		cloneflags |= syscall.CLONE_NEWIPC
	}

	if *hostname != "" {
		cloneflags |= syscall.CLONE_NEWUTS
	}

	state := &stateT{
		verbose:    *verbose,
		hostname:   *hostname,
		etc:        *etc,
		tmp:        *tmp,
		lib:        filterPathExists(strings.Split(*lib, ":")),
		cloneflags: cloneflags,
		cwd:        cwd,
	}

	state.run()
}

func (state *stateT) run() {
	path := filterPathExists(strings.Split(os.Getenv("PATH"), ":"))

	if root, ok := os.LookupEnv("NEWROOT_EXEC"); ok {
		if err := os.Unsetenv("NEWROOT_EXEC"); err != nil {
			log.Fatalln(err)
		}

		status, err := state.pivotRootAndExec(root, path)
		if err != nil {
			fmt.Fprintf(os.Stderr, "%s: %v\n", flag.Arg(0), err)
		}
		os.Exit(status)
	}

	if flag.NArg() == 0 {
		flag.Usage()
		os.Exit(2)
	}

	root, err := os.MkdirTemp("", "newroot")
	if err != nil {
		log.Fatalln(err)
	}

	if err := os.Setenv("NEWROOT_EXEC", root); err != nil {
		log.Fatalln(err)
	}

	status, err := state.fork(root, os.Args)
	if err != nil {
		fmt.Fprintf(os.Stderr, "%s: %v\n", flag.Arg(0), err)
	}
	os.Exit(status)
}

func (state *stateT) fork(root string, argv []string) (int, error) {
	defer func() {
		if err := os.RemoveAll(root); err != nil {
			log.Println(root, err)
		}
	}()

	log.Println(argv)

	cmd := exec.Command("/proc/self/exe", argv[1:]...)
	cmd.SysProcAttr = &syscall.SysProcAttr{
		Pdeathsig:  syscall.SIGKILL,
		Cloneflags: uintptr(state.cloneflags),
		UidMappings: []syscall.SysProcIDMap{
			{
				ContainerID: 0,
				HostID:      os.Getuid(),
				Size:        1,
			},
		},
		GidMappings: []syscall.SysProcIDMap{
			{
				ContainerID: 0,
				HostID:      os.Getgid(),
				Size:        1,
			},
		},
	}

	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	if err := cmd.Start(); err != nil {
		return 128, err
	}

	waitCh := make(chan error, 1)

	go func() {
		waitCh <- cmd.Wait()
	}()

	sigChan := make(chan os.Signal, 1)

	signal.Notify(sigChan)

	var exitError *exec.ExitError

	for {
		select {
		case sig := <-sigChan:
			_ = cmd.Process.Signal(sig)
		case err := <-waitCh:
			if err == nil {
				return 0, nil
			}

			if !errors.As(err, &exitError) {
				return 128, err
			}

			waitStatus, ok := exitError.Sys().(syscall.WaitStatus)
			if !ok {
				return 128, err
			}

			if waitStatus.Signaled() {
				return 128 + int(waitStatus.Signal()), nil
			}

			return waitStatus.ExitStatus(), nil
		}
	}
}

func subdir(dir string, path []string) bool {
	if dir == "/" {
		return true
	}

	for _, v := range path {
		rel, err := filepath.Rel(v, dir)
		if err != nil {
			// paths should be absolute
			log.Fatalln(err)
		}

		if strings.HasPrefix(rel, ".") {
			continue
		}

		log.Println("subdir", v, rel, dir)
		return true
	}

	return false
}

func fdopen(arg0 string) (uintptr, int, error) {
	exe, err := exec.LookPath(arg0)
	if err != nil {
		return 0, 127, err
	}

	file, err := os.Open(exe)
	if err != nil {
		return 0, 126, err
	}

	return file.Fd(), 0, nil
}

func (state *stateT) pivotRootAndExec(root string, path []string) (int, error) {
	fd, status, err := fdopen(flag.Arg(0))
	if err != nil {
		return status, err
	}

	if state.hostname != "" {
		if err := syscall.Sethostname([]byte(state.hostname)); err != nil {
			return 1, err
		}
	}

	dir := append(state.lib, path...)
	all := append(dir, sysdir...)

	log.Println("cwd", state.cwd)
	if !subdir(state.cwd, all) {
		all = append(all, state.cwd)
		state.mountCwd = true
		log.Println("cwd mount", state.mountCwd)
	}

	if err := state.chroot(root, all); err != nil {
		return 1, err
	}

	if err := state.pivotRoot(root, dir); err != nil {
		return 1, err
	}

	return 126, execve.Fexecve(fd, flag.Args(), syscall.Environ())
}

func (state *stateT) chroot(root string, path []string) error {
	for _, v := range path {
		dir := filepath.Join(root, v)
		log.Println("creating", v, "->", dir)
		if err := os.MkdirAll(dir, 0755); err != nil {
			return fmt.Errorf("%s: %w", dir, err)
		}
	}

	return nil
}

func bindmount(root string, path []string) error {
	for _, source := range path {
		target := filepath.Join(root, source)
		log.Println("mounting", source, "->", target)
		if err := syscall.Mount(source, target, "", syscall.MS_BIND, ""); err != nil {
			return fmt.Errorf("mount(%s, %s, MS_BIND): %w", source, target, err)
		}
		if err := syscall.Mount(source, target, "", syscall.MS_REMOUNT|syscall.MS_BIND|syscall.MS_RDONLY, ""); err != nil {
			return fmt.Errorf("mount(%s, %s, MS_REMOUNT|MS_BIND|MS_RDONLY): %w", source, target, err)
		}
	}

	return nil
}

func (state *stateT) pivotRoot(root string, path []string) error {
	if err := syscall.Mount("none", "/", "", syscall.MS_REC|syscall.MS_PRIVATE, ""); err != nil {
		return fmt.Errorf("mount(/, MS_REC|MS_PRIVATE): %w", err)
	}

	if err := syscall.Mount(root, root, "", syscall.MS_BIND, ""); err != nil {
		return fmt.Errorf("mount(%s, MS_BIND): %w", root, err)
	}

	if err := syscall.Mount(root, root, "", syscall.MS_REMOUNT|syscall.MS_BIND|syscall.MS_RDONLY, ""); err != nil {
		return fmt.Errorf("mount(%s, MS_REMOUNT|MS_BIND|MS_RDONLY): %w", root, err)
	}

	if err := bindmount(root, path); err != nil {
		return fmt.Errorf("mount(%s, MS_BIND): %w", root, err)
	}

	if state.mountCwd {
		if err := syscall.Mount(state.cwd, filepath.Join(root, state.cwd), "", syscall.MS_BIND, ""); err != nil {
			return fmt.Errorf("mount(%s, MS_BIND): %w", filepath.Join(root, state.cwd), err)
		}
	}

	if err := syscall.Mount("/dev", filepath.Join(root, "dev"), "", syscall.MS_BIND|syscall.MS_REC, ""); err != nil {
		return fmt.Errorf("mount(%s/dev, MS_BIND): %w", root, err)
	}

	if state.cloneflags&syscall.CLONE_NEWPID == 0 {
		if err := syscall.Mount("/proc", filepath.Join(root, "proc"), "", syscall.MS_BIND|syscall.MS_REC, ""); err != nil {
			return fmt.Errorf("mount(%s/proc, MS_BIND): %w", root, err)
		}
	} else {
		if err := syscall.Mount("proc", filepath.Join(root, "proc"), "proc", syscall.MS_NOEXEC|syscall.MS_NOSUID|syscall.MS_NODEV, ""); err != nil {
			return fmt.Errorf("mount(%s, MS_NOEXEC|MS_NOSUID|MS_NODEV): %w", filepath.Join(root, "proc"), err)
		}
	}

	if err := syscall.Mount("tmpfs", filepath.Join(root, "tmp"), "tmpfs", syscall.MS_NOEXEC|syscall.MS_NOSUID|syscall.MS_NODEV|syscall.MS_NOATIME, fmt.Sprintf("mode=1777,size=%s", state.tmp)); err != nil {
		return fmt.Errorf("mount(%s, MS_NOEXEC|MS_NOSUID|MS_NODEV|MS_NOATIME): %w", filepath.Join(root, "tmp"), err)
	}

	if err := syscall.Mount(state.etc, filepath.Join(root, "etc"), "", syscall.MS_BIND, ""); err != nil {
		return fmt.Errorf("mount(%s/etc, MS_BIND): %w", root, err)
	}
	if err := syscall.Mount(state.etc, filepath.Join(root, "etc"), "", syscall.MS_REMOUNT|syscall.MS_BIND|syscall.MS_RDONLY, ""); err != nil {
		return fmt.Errorf("mount(%s/etc, MS_REMOUNT|MS_BIND|MS_RDONLY): %w", root, err)
	}

	if err := syscall.Chdir(root); err != nil {
		return fmt.Errorf("chdir(%s): %w", root, err)
	}

	if err := syscall.PivotRoot(".", "."); err != nil {
		return fmt.Errorf("pivot_root(%s): %w", root, err)
	}

	if err := syscall.Unmount(".", syscall.MNT_DETACH); err != nil {
		return fmt.Errorf("umount2(%s, MNT_DETACH): %w", root, err)
	}

	if err := syscall.Chdir(state.cwd); err != nil {
		return fmt.Errorf("chdir: %s/%s: %w", root, state.cwd, err)
	}

	return nil
}
